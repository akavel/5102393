@echo off
:: Homepage: https://gist.github.com/akavel/5102393
:: Author:   (c) 2013 by Mateusz Czaplinski
:: License:  MIT/X11
if "%1"=="clone" goto :clone
if "%1"=="show-ref" goto :showref
if "%1"=="checkout" goto :checkout
goto :unknown

:clone
call hg clone git+%2.git %3
goto :eof

:showref
call hg parent --template "{node} refs/heads/master\n"
goto :eof

:checkout
if not "%2"=="origin/master" goto :unknown
goto :eof

:unknown
echo GIT %*
goto :err
:err
cmd /c exit /b 1
